﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace Oleksandr_Mykoliuk.RobotChallange
{
    [TestClass]
    public class TestAlgorithm
    {
        [TestMethod]
        public void TestMoveCommand()//чи буде робот рухатися до станції
        {
            var algorithm = new OleksandrMykoliukAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1,1);
            map.Stations.Add(new EnergyStation() {Energy = 1000, Position = stationPosition, RecoveryRate = 2});
            var robots = new List <Robot.Common.Robot>() {new Robot.Common.Robot() {Energy=200, Position = new Position(2,3)}};
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand)command).NewPosition, stationPosition);
        }

        [TestMethod]
        public void TestDefaultCommand()
        {
            var algorithm = new OleksandrMykoliukAlgorithm();
            var map = new Map();
            var robots = new List<Robot.Common.Robot>
            {
                new Robot.Common.Robot() { Energy = 100500, Position = new Position(1, 1) }
            };
            var command = algorithm.DoStep(robots, 0, map);
            Assert.IsNull(command);
        }

        [TestMethod]
        public void TestCollectCommand()//чи буде робот рухатися до станції
        {
            var algorithm = new OleksandrMykoliukAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 200, Position = new Position(1, 1) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CollectEnergyCommand);    
        }
        [TestMethod]
        public void TesCreateNewRobotCommand()
        {
            var algorithm = new OleksandrMykoliukAlgorithm();
            var map = new Map();

            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(1, 1),
                RecoveryRate = 2
            });
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(2, 2),
                RecoveryRate = 2
            });
            var robots = new List<Robot.Common.Robot>
            {
                new Robot.Common.Robot() { Energy = 100500, Position = new Position(0, 0) }
            };
            var command = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(command is CreateNewRobotCommand);
        }

        [TestMethod]
        public void TestIsCellFree()
        {
            var algorithm = new OleksandrMykoliukAlgorithm();
            var robot1 = new Robot.Common.Robot() { Position = new Position(1, 1) };
            var robot2 = new Robot.Common.Robot() { Position = new Position(10, 10) };
            var robots = new List<Robot.Common.Robot>() { robot1, robot2 };
            Assert.IsFalse(algorithm.IsCellFree(new Position(1, 1), robot2, robots));
        }
        [TestMethod]
        public void TestGoToNextStationCommand()
        {
            var algorithm = new OleksandrMykoliukAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 0, Position = stationPosition, RecoveryRate = 2 });
            stationPosition = new Position(6, 7);
            map.Stations.Add(new EnergyStation() { Energy = 450, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot()
                {
                    Energy = 200,
                    Position = new Position(1, 1)
                }
            };

            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is MoveCommand);

        }


        [TestMethod]
        public void TestCreateNewRobotCommand()
        {
            var algorithm = new OleksandrMykoliukAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot()
                {
                    Energy = 500,
                    Position = new Position(1, 1)
                }
            };

            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CreateNewRobotCommand);

        }
        [TestMethod]
        public void TestCountMyRobotsNearTheStation()
        {
            var map = new Map();

            map.Stations.Add(new EnergyStation() { Position = new Position(3, 3) });

            Owner I = new Owner();
            Owner enemy = new Owner();

            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>
            {
                new Robot.Common.Robot() {Energy = 200, Position = new Position(5, 2), Owner = I},
                new Robot.Common.Robot() {Energy = 200, Position = new Position(2, 2), Owner = I},
                new Robot.Common.Robot() {Energy = 200, Position = new Position(3, 4), Owner = enemy},
                new Robot.Common.Robot() {Energy = 200, Position = new Position(2, 3), Owner = enemy},
                new Robot.Common.Robot() {Energy = 200, Position = new Position(2, 4), Owner = I}
            };

        }

        [TestMethod]
        public void TestEnergyinStationCommand()
        {
            var algorithm = new OleksandrMykoliukAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 250, Position = stationPosition, RecoveryRate = 2 });

            stationPosition = new Position(5, 6);
            map.Stations.Add(new EnergyStation() { Energy = 450, Position = stationPosition, RecoveryRate = 2 });

        }
        [TestMethod]
        public void TestIsStationFree()
        {
            var algorithm = new OleksandrMykoliukAlgorithm();
            var map = new Map();

            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(1, 1),
                RecoveryRate = 2
            });
            var robots = new List<Robot.Common.Robot>
            {
                new Robot.Common.Robot() { Energy = 100500, Position = new Position(0, 0) }
            };

            Assert.IsTrue(algorithm.IsStationFree(map.Stations[0], robots[0], robots));
        }
        [TestMethod]
        public void TestMapGetResource()
        {
            var map = new Map();

            map.Stations.Add(new EnergyStation()
            {
                Energy = 2000,
                Position = new Position(1, 1),
                RecoveryRate = 1
            });
        }
        }
}
